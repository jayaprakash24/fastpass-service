package com.jgeek.fastpass.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.function.Function;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FastPassCustomer {
    private Long id;
    private String name;
    private String phone;
    private BigDecimal balance;
    private Status status;

    public static Function<String, FastPassCustomer> FAST_PASS_CUSTOMER_BUILDER = s -> {
        String[] tokens = s.split(",");
        FastPassCustomer fastPassCustomer = new FastPassCustomer();
        fastPassCustomer.setId(Long.valueOf(tokens[0]));
        fastPassCustomer.setName(tokens[1]);
        fastPassCustomer.setPhone(tokens[2]);
        fastPassCustomer.setBalance(new BigDecimal(tokens[3]));
        fastPassCustomer.setStatus(Status.valueOf(tokens[4]));
        return fastPassCustomer;
    };
}
