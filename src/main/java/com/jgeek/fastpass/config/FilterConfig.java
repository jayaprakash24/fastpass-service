package com.jgeek.fastpass.config;

import com.jgeek.fastpass.filter.RequestResponseLoggingFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean<RequestResponseLoggingFilter> loggingFilter() {
        FilterRegistrationBean<RequestResponseLoggingFilter> filterRegBean
            = new FilterRegistrationBean<>();
        filterRegBean.setName("Request/Response Logging Filter");
        filterRegBean.setFilter(new RequestResponseLoggingFilter());
        filterRegBean.addUrlPatterns("/fastpasscustomers/*");
        return filterRegBean;
    }
}
