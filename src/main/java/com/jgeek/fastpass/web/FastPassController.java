package com.jgeek.fastpass.web;

import com.jgeek.fastpass.model.FastPassCustomer;
import com.jgeek.fastpass.processor.FileProcessor;
import com.jgeek.fastpass.service.FastPassService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.jgeek.fastpass.model.FastPassCustomer.FAST_PASS_CUSTOMER_BUILDER;

@Slf4j
@RestController
public class FastPassController {

    private FastPassService fastPassService;

    public FastPassController(FastPassService fastPassService) {
        this.fastPassService = fastPassService;
    }

    @GetMapping("/fastpasscustomers/{id}")
    public ResponseEntity<?> getFastPassCustomerDetails(@PathVariable("id") Long id) {
        Optional<FastPassCustomer> fastPassCustomer = fastPassService.getFastPassCustomer(id);
        return fastPassCustomer.isPresent() ?
            ResponseEntity.ok(fastPassCustomer.get()) : ResponseEntity.notFound().build();
    }

    @PostMapping("/fastpasscustomers/upload")
    public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        List result = FileProcessor.processCsv(file.getInputStream(), FAST_PASS_CUSTOMER_BUILDER);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/fastpasscustomers/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id) {
        throw new NotImplementedException();
    }

    @DeleteMapping("/fastpasscustomers/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        throw new NotImplementedException();
    }
}
