package com.jgeek.fastpass.model;

public enum Status {
    ACTIVE,
    INACTIVE,
    SUSPENDED
}
