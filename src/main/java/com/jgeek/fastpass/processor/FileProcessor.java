package com.jgeek.fastpass.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface FileProcessor {

    <T> List<T> process(InputStream inputStream);

    static <T> T processJson(InputStream inputStream, CollectionType collectionType) throws IOException {
        return new ObjectMapper().readValue(inputStream, collectionType);
    }

    static <T> T processJson(String s, Class<T> cls) throws IOException {
        return new ObjectMapper().readValue(s, cls);
    }

    static <T> List<T> processCsv(InputStream inputStream, Function<String, T> function) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            return br.lines().map(function).collect(Collectors.toList());
        }
    }
}
