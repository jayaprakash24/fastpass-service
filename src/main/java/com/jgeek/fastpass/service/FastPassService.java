package com.jgeek.fastpass.service;

import com.jgeek.fastpass.model.FastPassCustomer;
import com.jgeek.fastpass.model.Status;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class FastPassService {

    private Map<Long, FastPassCustomer> fastPassCustomers = new HashMap<>();

    @PostConstruct
    public void initialize() {
        fastPassCustomers.put(100L, new FastPassCustomer(100L, "Tony Stark", "555-123-4567",
            new BigDecimal("19.50"), Status.ACTIVE));
        fastPassCustomers.put(101L, new FastPassCustomer(101L, "Steve Rogers", "555-123-4567",
            new BigDecimal("19.50"), Status.INACTIVE));
    }

    public Optional<FastPassCustomer> getFastPassCustomer(Long id) {
        if (fastPassCustomers.containsKey(id)) {
            return Optional.of(fastPassCustomers.get(id));
        } else {
            return Optional.empty();
        }
    }
}
