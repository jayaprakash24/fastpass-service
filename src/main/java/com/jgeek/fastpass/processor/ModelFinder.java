package com.jgeek.fastpass.processor;

import com.jgeek.fastpass.model.FastPassCustomer;

import java.util.HashMap;
import java.util.Map;

public class ModelFinder {

    Map<String, Class> classes = new HashMap<>();

    public void initialize() {
        classes.put("FastPassCustomer", FastPassCustomer.class);
    }
    public Class getClass(String name) {
        return (Class) classes.get(name);
    }
}
